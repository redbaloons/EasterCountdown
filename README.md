C:\Sites>cat C:\Users\Kasia/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDipsaJwGi9OgBzTAXNFIhP0n57VP4nYePcesPxrtv/iX4EhQrnm0iXj3L9D9FDkK+RZTuiM9f5bYB7lWS9r9+GZOtnHXbf3XNqIrVNWs4Mu5rSE6Kxes0kx/vcjhWtDSC
Y5A7ftbrHIcp0sOmQYFazIRFoDXJl4XRJStcmfByH+mQqvr6B4duJO7/HTQ1gdbLZo6tfk1ysUGxTFYmM1w6ed6Ng5fFGk9lru96nFF8s/6vUPvX8q+OYwdmXbefbfbXnWMSdnjx27cPrFYQWKyd0zsej+EKnG1hdcBmihy
4eQnfm9citPVScc9yt2x Katarzyna Popek <kasia@nukomeet.com>

C:\Sites>git remote --help
Launching default browser to display HTML ...

C:\Sites\EasterCountdown>git remote set-url origin git@gitlab.com:redbaloons/EasterCountdown.git

C:\Sites\EasterCountdown>explorer

C:\Sites\EasterCountdown>atom .

C:\Sites\EasterCountdown>git pull
The authenticity of host 'gitlab.com (104.210.2.228)' can't be established.
ECDSA key fingerprint is f1:d0:fb:46:73:7a:70:92:5a:ab:5d:ef:43:e2:1c:35.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitlab.com,104.210.2.228' (ECDSA) to the list of known hosts.
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From gitlab.com:redbaloons/EasterCountdown
   6f85636..46583ba  master     -> origin/master
Updating 6f85636..46583ba
Fast-forward
 index.html | 1 +
 1 file changed, 1 insertion(+)
 create mode 100644 index.html

C:\Sites\EasterCountdown> git add .

C:\Sites\EasterCountdown> git commit -m "dodany java script"
[master 2b912fd] dodany java script
 1 file changed, 5 insertions(+), 1 deletion(-)

C:\Sites\EasterCountdown> git push -u origin master
Branch master set up to track remote branch master from origin.
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 399 bytes | 0 bytes/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To git@gitlab.com:redbaloons/EasterCountdown.git
   46583ba..2b912fd  master -> master

C:\Sites\EasterCountdown> git push
Everything up-to-date

C:\Sites\EasterCountdown>git pull
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From gitlab.com:redbaloons/EasterCountdown
   2b912fd..138c50c  master     -> origin/master
Updating 2b912fd..138c50c
Fast-forward
 index.html | 4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)

C:\Sites\EasterCountdown>git pull
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From gitlab.com:redbaloons/EasterCountdown
   138c50c..9a7ac36  master     -> origin/master
Updating 138c50c..9a7ac36
Fast-forward
 index.html | 4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)
